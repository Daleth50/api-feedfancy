import requests
import json

from feedfancy.settings import TOKENBIT


class Short:
    @classmethod
    def shorter(cls, url: str):
        """
        Metodo que recibe una URL y regresa una URL acortada con bitly

        :param url:
        :return:
        """
        query_params = {
            'access_token': TOKENBIT,
            'longUrl': url
        }
        endpoint = 'https://api-ssl.bitly.com/v3/shorten'
        response = requests.get(endpoint, params=query_params, verify=True)
        data = json.loads(response.content)
        return data.get('data').get('url')

    @classmethod
    def number_click(cls, url: str):
        """
        Metodo que recibe una URL corta y regresa el numero de clicks de la misma

        :param url:
        :return:
        """
        query_params = {
            'access_token': TOKENBIT,
            'link': url
        }
        endpoint = 'https://api-ssl.bitly.com/v3/link/clicks'
        response = requests.get(endpoint, params=query_params, verify=True)
        data = json.loads(response.content)
        return data.get('data').get('link_clicks')
