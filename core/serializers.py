from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed
from core.models import Post, Taxonomy, Source


class PublicTaxonomySerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomy
        fields = ['slug', 'name']


class PublicSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ['name', 'url']


class PublicPostSerializer(serializers.ModelSerializer):
    taxonomies = PublicTaxonomySerializer(many=True, read_only=True)
    source = PublicSourceSerializer(many=False, read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'slug', 'title', 'content', 'summary', 'link', 'imgUrl', 'taxonomies', 'source']

    def create(self, validated_data):
        raise MethodNotAllowed('Solicitud no valida')

    def update(self, instance, validated_data):
        raise MethodNotAllowed('Solicitud no valida')
