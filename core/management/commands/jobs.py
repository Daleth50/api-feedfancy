from django_rq import enqueue
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('op', nargs='+', type=str)

    def handle(self, *args, **options):
        if options['op'][0] == 'getPosts':
            enqueue('send_spider')
