from django.contrib import admin
from core.models import Taxonomy, Source, SourceUrl, Post


# Register models for django admin and build CRUD
@admin.register(Taxonomy)
class TaxonomyAdmin(admin.ModelAdmin):
    pass
    # auto fill the slug field with pre populated fields
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    # friendly url in the admin
    list_display = ('name', 'isActive')
    list_editable = ('isActive',)


@admin.register(SourceUrl)
class SourceUrlAdmin(admin.ModelAdmin):
    # friendly url in the admin
    list_display = ('source', 'url', 'isActive')
    list_editable = ('isActive',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'link')
